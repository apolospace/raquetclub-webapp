import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent {

  public isHandset: boolean;
	public mode: string;
	public role: string;
	private subscription: Subscription;

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit(): void {
		this.breakpointObserver.observe(Breakpoints.Handset).subscribe((result) => {
			this.isHandset = result.matches;
			this.role =  this.isHandset ? 'dialog' : 'navigation';
			this.mode = this.isHandset ? 'over' : 'side';
		});
	}

	ngOnDestroy(): void {
		if(this.subscription) {
			this.subscription.unsubscribe();
		}
	}

}
